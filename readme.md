# Java No-SQL
Helper project that connects to DynamoDB.

# Links
[YouTube](https://www.youtube.com/watch?v=amlQdiJIGpg)


https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/DynamoDBLocal.DownloadingAndRunning.html

// https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/ql-gettingstarted.html

[PartiQL](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/ql-gettingstarted.html)