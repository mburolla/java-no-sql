package NoSQL;

import Models.Person;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.services.dynamodbv2.model.*;

import java.util.List;
import java.util.ArrayList;

public class DataAccess {

    //
    // Data members
    //

    private final Table personTable;

    //
    // Constructors
    //

    public DataAccess() {
        DynamoDB dynamoDB = new DynamoDB(Regions.US_EAST_1); // Not using a local build of DynamoDB.
        this.personTable = dynamoDB.getTable("Person");
    }

    //
    // Methods
    //

    public void addPerson(Person person) throws Exception {
        try {
            var putItemOutcome = this.personTable.putItem(
                    new Item().withPrimaryKey("PersonId", person.getPersonId())
                            .with("FirstName", person.getFirstName())
                            .with("LastName", person.getLastName())
                            .withJSON("Age", Integer.toString(person.getAge())));
            var putItemResult = putItemOutcome.getPutItemResult();
            if (putItemResult.getSdkHttpMetadata().getHttpStatusCode() == 200) {
                System.out.println(String.format("Added: %s.", person));
            }
        }
        catch(Exception e) {
            System.out.println(e);
        }
    }

    public Person getPerson(int personId) {
        Person retval = new Person();
        GetItemSpec spec = new GetItemSpec().withPrimaryKey("PersonId", personId);
        Item outcome = this.personTable.getItem(spec);
        retval.setPersonId(Integer.parseInt(outcome.getJSON("PersonId").toString()));
        retval.setFirstName(outcome.get("FirstName").toString());
        retval.setLastName(outcome.get("LastName").toString());
        retval.setAge(Integer.parseInt(outcome.get("Age").toString()));
        return retval;
    }

    //
    // PartiQL
    //

    public List<Person> getPersons(int personId) {
        var retval = new ArrayList<Person>();

        // Query DynamoDB.
        AmazonDynamoDB dynamoDB = AmazonDynamoDBClientBuilder.standard().withRegion(Regions.US_EAST_1).build();
        ExecuteStatementRequest request = new ExecuteStatementRequest();
        request.setStatement("select * from Person where PersonId > ?");
        List<AttributeValue> parameters = new ArrayList<AttributeValue>();
        parameters.add(new AttributeValue().withN(Integer.toString(personId))); // IMPORTANT: withN()
        request.setParameters(parameters);
        var executeStatementResult = dynamoDB.executeStatement(request);

        // Convert results from DynamoDB to POJO.
        var iterator = executeStatementResult.getItems().iterator();
        while (iterator.hasNext()) {
            var map = iterator.next();
            var pId = Integer.parseInt(map.get("PersonId").getN());
            var firstName = map.get("FirstName").getS();
            var lastName = map.get("LastName").getS();
            var age = Integer.parseInt(map.get("Age").getN());
            var p = new Person(pId, firstName, lastName, age);
            retval.add(p);
        }
        return retval;
    }
}
