package NoSQL;

import Models.Person;

public class Main {
    public static void main(String[] args) throws Exception {

        Person newPerson1 = new Person(1, "Alice", "Chang", 10);
        Person newPerson2 = new Person(2, "Bob", "Jone", 20);
        Person newPerson3 = new Person(3, "Charlie", "Smith", 30);
        Person newPerson4 = new Person(4, "Dave", "Campbell", 40);

        var ddbDataAccess = new DataAccess();

//        ddbDataAccess.addPerson(newPerson1);
//        ddbDataAccess.addPerson(newPerson2);
//        ddbDataAccess.addPerson(newPerson3);
//        ddbDataAccess.addPerson(newPerson4);

//        var recallPerson = ddbDataAccess.getPerson(4);
//        System.out.println(recallPerson);

        var personList = ddbDataAccess.getPersons(2);
        for (Person p: personList) {
            System.out.println(p);
        }
    }
}
