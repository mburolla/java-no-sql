package Models;

public class Person implements Comparable {

    //
    // Data members.
    //

    private int age;
    private int personId;
    private String firstName;
    private String lastName;

    //
    // Constructors
    //

    public Person() {

    }

    public Person(int personId, String firstName, String lastName, int age) {
        this.age = age;
        this.personId = personId;
        this.lastName = lastName;
        this.firstName = firstName;
    }

    //
    // Accessors
    //

    public int getAge() {
        return age;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String secondName) {
        this.lastName = secondName;
    }

    @Override
    public String toString() {
        return String.format("Person ID: %s, Name: %s %s, Age: %s", this.personId, this.firstName, this.lastName, this.age);
    }

    @Override
    public int compareTo(Object o) {
        int retval = 0;
        Person p = (Person)o;
        if (this.getPersonId() == p.personId) {
            retval = 1;
        }
        return retval;
    }
}
